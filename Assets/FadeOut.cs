using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour
{
    public float duration;
    public bool destroyAfterFadingOut;
    private void Start()
    {
        GetComponent<Image>().CrossFadeAlpha(0, duration, false);

        if (destroyAfterFadingOut)
            Destroy(gameObject, duration);
    }
}
