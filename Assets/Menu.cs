using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [SerializeField] private SfxPlayer sfx;

    public void StartGame()
    {
        sfx.PlaySfx("btn-tap-2");
        AppRoot.Instance.GetService<Transitioner>().TransitionToScene("Level_Intro");
    }

    private void Start()
    {
        AppRoot.Instance?.GetService<BgmPlayer>().LoopBgm("bgm-menu");
    }
}
