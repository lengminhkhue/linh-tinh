using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Messaging : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI chatInput, typing;
    [SerializeField] private GameObject chatBubble, typingBubble, replyBubble, readTxt;
    [SerializeField] private Button btnSend;
    [SerializeField] private GameObject ending;
    [SerializeField] private SfxPlayer sfx;

    private void Start()
    {
        chatBubble.SetActive(false);
        typingBubble.SetActive(false);
        replyBubble.SetActive(false);
        readTxt.SetActive(false);

        btnSend.gameObject.SetActive(false);
        StartCoroutine(MessageInput());
    }

    public void SendMessage()
    {
        chatBubble.SetActive(true);
        chatInput.text = "";
        btnSend.interactable = false;
        sfx.PlaySfx("textbox-send");

        StartCoroutine(AnimProgress());
    }

    private IEnumerator MessageInput()
    {
        string text = chatInput.text;
        string temp = "";
        chatInput.text = "|";

        yield return new WaitForSeconds(3f);

        chatInput.text = "";
        sfx.PlaySfx("message-keyboard");

        foreach (var c in text)
        {
            temp += c;
            chatInput.text = temp + "|";
            yield return new WaitForSeconds(0.3f);
        }

        yield return new WaitForSeconds(1f);
        btnSend.gameObject.SetActive(true);
    }

    private IEnumerator TypingBubble()
    {
        typing.text = "";
        while (true)
        {
            if (typing.text.Length >= 3)
                typing.text = "";

            typing.text += ".";
            yield return new WaitForSeconds(0.2f);
        }
    }

    private IEnumerator AnimProgress()
    {
        yield return new WaitForSeconds(2f);
        readTxt.SetActive(true);

        yield return new WaitForSeconds(1f);
        typingBubble.SetActive(true);
        StartCoroutine(TypingBubble());

        yield return new WaitForSeconds(2f);
        typingBubble.SetActive(false);

        yield return new WaitForSeconds(1f);
        typingBubble.SetActive(true);

        yield return new WaitForSeconds(2f);
        typingBubble.SetActive(false);

        yield return new WaitForSeconds(2f);
        typingBubble.SetActive(false);

        yield return new WaitForSeconds(1f);
        typingBubble.SetActive(true);

        yield return new WaitForSeconds(2f);
        typingBubble.SetActive(false);

        yield return new WaitForSeconds(0.5f);
        replyBubble.SetActive(true);
        sfx.PlaySfx("textbox-receive");

        yield return new WaitForSeconds(2f);
        ending.SetActive(true);
    }
}
