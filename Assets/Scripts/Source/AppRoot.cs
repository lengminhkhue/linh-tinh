using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Framework.ServiceLocator;
using UnityEngine.UI;

public class AppRoot : ServiceLocator
{
    public string startSceneName;

    private void Start()
    {
        Application.targetFrameRate = 60;
        DontDestroyOnLoad(gameObject);

        GetService<Transitioner>().TransitionToScene(startSceneName);
    }
}
