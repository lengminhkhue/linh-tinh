using Framework.ServiceLocator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgmPlayer : Service
{
    public List<AudioClipInfo> bgm;

    [SerializeField] private AudioSource source;

    public void LoopBgm(string id)
    {
        StopLoopingBgm();
        if (clips.ContainsKey(id))
        {
            source.loop = true;
            source.clip = clips[id];
            source.Play();
        }
    }

    public void StopLoopingBgm()
    {
        source.Stop();
    }

    private void Awake()
    {
        clips = new Dictionary<string, AudioClip>();
        foreach (var c in bgm)
        {
            clips.Add(c.id, c.clip);
        }
    }

    private Dictionary<string, AudioClip> clips;
}
