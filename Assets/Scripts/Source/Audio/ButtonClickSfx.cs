using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClickSfx : MonoBehaviour
{
    [SerializeField] private AudioSource source;
    public AudioClip clip;

    public void OnClick()
    {
        source.PlayOneShot(clip);
    }
}
