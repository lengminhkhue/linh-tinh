using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioClipInfo
{
    public string id;
    public AudioClip clip;
}

public class SfxPlayer : MonoBehaviour
{
    public List<AudioClipInfo> sfx;

    [SerializeField] private AudioSource source;

    public void PlaySfx(string id)
    {
        if (clips.ContainsKey(id))
        {
            source.PlayOneShot(clips[id]);
        }
    }

    public void LoopSfx(string id)
    {
        StopLoopingSfx();
        if (clips.ContainsKey(id))
        {
            source.loop = true;
            source.clip = clips[id];
            source.Play();
        }
    }

    public void StopLoopingSfx()
    {
        source.Stop();
    }

    private void Awake()
    {
        clips = new Dictionary<string, AudioClip>();
        foreach (var c in sfx)
        {
            clips.Add(c.id, c.clip);
        }
    }

    private Dictionary<string, AudioClip> clips;
}
