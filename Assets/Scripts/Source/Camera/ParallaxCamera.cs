using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxCamera : MonoBehaviour
{
	public List<ParallaxLayer> layers;
	private Vector3 oldPosition;

	void Start()
	{
		oldPosition = transform.position;
	}

	void Update()
	{
		if (transform.position != oldPosition)
		{
			foreach (var layer in layers)
			{
				Vector3 delta = oldPosition - transform.position;
				layer.Move(delta);
			}

			oldPosition = transform.position;
		}
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Trigger"))
        {
			collision.GetComponent<Trigger>().TriggerEvent();
        }
    }
}