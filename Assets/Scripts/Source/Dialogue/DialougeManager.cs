using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

[System.Serializable]
public class DialogueItem
{
    public string speakerId;
    [TextArea] public string content;
}

[System.Serializable]
public class DialogueSpeaker
{
    public string speakerId;
    public GameObject speech;
    public TMPro.TextMeshProUGUI text;
}

public class DialougeManager : MonoBehaviour
{
    public List<DialogueSpeaker> speakers;
    public List<DialogueItem> speakSequence;
    public UnityEvent endDialougueEvent;
    public bool autoStart;

    private bool isTyping;
    private int currentIndex;

    private void Awake()
    {
        isTyping = false;
    }

    private void Start()
    {
        currentIndex = 0;
        if (autoStart)
        {
            StartCoroutine(StartDialogue());
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!isTyping)
            {
                if (currentIndex >= speakSequence.Count)
                {
                    endDialougueEvent.Invoke();
                    gameObject.SetActive(false);
                }
                else
                {
                    DialogueItem item = speakSequence[currentIndex];
                    DialogueSpeaker speaker = speakers.Find(x => x.speakerId.Equals(item.speakerId));
                    speaker.speech.transform.localScale = Vector3.zero;

                    currentIndex++;
                    if (currentIndex < speakSequence.Count)
                        StartCoroutine(StartDialogue());
                }
            }
        }
    }

    private IEnumerator StartDialogue()
    {
        DialogueItem item = speakSequence[currentIndex];
        DialogueSpeaker speaker = speakers.Find(x => x.speakerId.Equals(item.speakerId));
        speaker.speech.transform.localScale = Vector3.zero;
        speaker.speech.transform.DOScale(Vector3.one, 0.2f);
        speaker.text.text = string.Empty;
        isTyping = true;
        yield return new WaitForSeconds(0.2f);

        foreach (var letter in item.content)
        {
            speaker.text.text += letter;
            yield return new WaitForSeconds(0.05f);
        }

        yield return new WaitForSeconds(1f);
        isTyping = false;
    }
}
