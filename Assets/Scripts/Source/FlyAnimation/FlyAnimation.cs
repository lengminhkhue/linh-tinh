using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FlyAnimation : MonoBehaviour
{
    public enum FlyType
    {
        In, Out
    }

    public FlyType type;
    public float duration;
    public Ease easeFunction;
    public float delay;
    public Transform startPosition;

    private void Start()
    {
        var endPosition = transform.position;
        transform.position = startPosition.position;
        transform.DOMove(endPosition, duration, false).SetEase(easeFunction);
    }
}
