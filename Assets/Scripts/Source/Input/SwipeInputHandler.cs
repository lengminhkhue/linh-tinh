using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SwipeInputHandler : MonoBehaviour
{
    public float panSpeed = 5f;
    public bool horizontal, vertical;
    public bool sceneSnapping;
    public Vector3[] snapPositions;
    public Vector2 minBound, maxBound;

    [SerializeField] private Camera camera;
    private Vector3 origin;
    private Vector3 dragDiff, newPos;

    private void Start()
    {
        newPos = camera.transform.position;
    }

    public void SetPosition(Vector2 pos)
    {
        newPos.x = pos.x;
        newPos.y = pos.y;
        camera.transform.position = newPos;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            origin = camera.ScreenToWorldPoint(Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {
            dragDiff = origin - camera.ScreenToWorldPoint(Input.mousePosition);
            newPos = camera.transform.position + dragDiff;

            if (sceneSnapping)
            {
                int index = GetCurrentSceneSnap(newPos);
                if (index >= 0)
                {
                    var snapPos = snapPositions[index];
                    newPos = snapPos;
                }
            }

            if (!horizontal)
                newPos.x = camera.transform.position.x;

            if (!vertical)
                newPos.y = camera.transform.position.y;

            newPos.z = -10;
        }

        newPos.x = Mathf.Clamp(newPos.x, minBound.x, maxBound.x);
        newPos.y = Mathf.Clamp(newPos.y, minBound.y, maxBound.y);

        var pos = Vector3.Lerp(camera.transform.position, newPos, panSpeed * Time.deltaTime);
        camera.transform.position = pos;
    }

    private int GetCurrentSceneSnap(Vector3 pos)
    {
        for (int i = 0; i < snapPositions.Length - 1; ++i)
        {
            if (horizontal)
            {
                if (snapPositions[i].x <= pos.x && pos.x <= snapPositions[i + 1].x)
                {
                    return i;
                }
            }
            else if (vertical)
            {
                if (snapPositions[i].y <= pos.y && pos.y <= snapPositions[i + 1].y)
                {
                    return i;
                }
            }
        }

        return -1;
    }
}
