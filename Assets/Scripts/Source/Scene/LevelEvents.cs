using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelEvents : MonoBehaviour
{
    [SerializeField] protected SfxPlayer sfx;

    public void FadeInGraphic(SpriteRenderer renderer, float duration)
    {
        var color = renderer.color;
        color.a = 0f;
        renderer.color = color;
        renderer.DOColor(new Color(color.r, color.g, color.b, 1f), 2f);
    }
}
