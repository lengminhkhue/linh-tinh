using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// This script contains all the events that are referenced by the SceneTimeline
/// </summary>
public class LevelChapter1Events : LevelEvents
{
    public List<SpriteRenderer> sceneGraphics;
    public List<GameObject> shots;
    public Camera camera;

    [SerializeField] private Button moveOnButton;
    [SerializeField] private Button nextButton;
    [SerializeField] private SwipeInputHandler swipeInput;

    [SerializeField] private GameObject shot2, shot3;

    public void Shot1_1()
    {
        Debug.Log("Trigger shot 1-1");
        swipeInput.enabled = false;
        shot2.SetActive(false);
        shot3.SetActive(false);
        StartCoroutine(Shot1_1_Progress());
        sfx.PlaySfx("btn-tap-1");
    }

    public void Shot2_1()
    {
        moveOnButton.gameObject.SetActive(false);
        shot2.SetActive(true);
        SpriteRenderer bg53 = graphics["Shot2_Bg_5_3"];
        SpriteRenderer bg54 = graphics["Shot2_Bg_5_4"];
        bg53.gameObject.SetActive(false);
        bg54.gameObject.SetActive(false);
        StartCoroutine(Shot2_1_Progress());
    }

    public void EndShots()
    {
        SpriteRenderer bg53 = graphics["Shot2_Bg_5_3"];
        SpriteRenderer bg54 = graphics["Shot2_Bg_5_4"];

        bg53.gameObject.SetActive(true);
        //bg54.gameObject.SetActive(true);

        SpriteRenderer bg51 = graphics["Shot2_Bg_5_1"];
        SpriteRenderer bg52 = graphics["Shot2_Bg_5_2"];
        bg51.transform.DOMoveY(-50, 3f).SetEase(Ease.InOutSine);
        bg52.transform.DOMoveY(-47, 3f).SetEase(Ease.InOutSine);

        FadeInGraphic(bg53, 1f);
        FadeInGraphic(bg54, 1f);

        nextButton.gameObject.SetActive(true);
        nextButton.onClick.AddListener(Shot3_1);
    }

    public void Shot3_1()
    {
        sfx.PlaySfx("btn-tap-1");
        nextButton.gameObject.SetActive(false);

        SpriteRenderer bg = graphics["Shot3_Bg"];
        SpriteRenderer sf = graphics["Shot3_SketchFrame"];
        SpriteRenderer sc = graphics["Shot3_LastScene"];
        FadeInGraphic(bg, 2f);
        FadeInGraphic(sf, 2f);
        FadeInGraphic(sc, 2f);
        shot3.SetActive(true);

        swipeInput.enabled = false;
        camera.transform.DOMoveY(0, 2f).SetEase(Ease.OutSine);

        StartCoroutine(Shot3_1_Progress());
    }

    private IEnumerator Shot3_1_Progress()
    {
        yield return new WaitForSeconds(2f);
        nextButton.onClick.RemoveAllListeners();
        nextButton.onClick.AddListener(() =>
        {
            sfx.PlaySfx("btn-tap-1");
            AppRoot.Instance.GetService<Transitioner>().TransitionToScene("Level_Chapter1_2");
        });
        nextButton.gameObject.SetActive(true);
    }

    private IEnumerator Shot1_1_Progress()
    {
        SpriteRenderer bg = graphics["Shot1_Bg"];
        FadeInGraphic(bg, 2f);
        SpriteRenderer sf = graphics["Shot1_SketchFrame"];
        FadeInGraphic(sf, 2f);
        moveOnButton.gameObject.SetActive(false);

        yield return new WaitForSeconds(2f);

        moveOnButton.gameObject.SetActive(true);
        moveOnButton.onClick.RemoveAllListeners();
        moveOnButton.onClick.AddListener(Shot2_1);
    }

    private IEnumerator Shot2_1_Progress()
    {
        SpriteRenderer bg1 = graphics["Shot1_Bg"];
        SpriteRenderer sf1 = graphics["Shot1_SketchFrame"];

        bg1.transform.DOMoveX(-8.0f, 2f).SetEase(Ease.InOutSine);
        sf1.transform.DOMoveX(-8.0f, 2f).SetEase(Ease.InOutSine);

        sfx.LoopSfx("lecture-ambience");

        yield return new WaitForSeconds(1f);

        SpriteRenderer bg2 = graphics["Shot2_Bg"];
        SpriteRenderer sf2 = graphics["Shot2_SketchFrame"];
        SpriteRenderer vfx2 = graphics["Shot2_Vfx"];

        bg2.transform.position = new Vector2(8, 0);
        sf2.transform.position = new Vector2(8, 0);
        bg2.transform.DOMoveX(0f, 2f).SetEase(Ease.InOutSine);
        sf2.transform.DOMoveX(0f, 2f).SetEase(Ease.InOutSine);
        FadeInGraphic(vfx2, 2f);

        swipeInput.enabled = true;
    }

    private void Start()
    {
        AppRoot.Instance?.GetService<BgmPlayer>().LoopBgm("bgm-nostalgia");
    }

    private void Awake()
    {
        graphics = new Dictionary<string, SpriteRenderer>();
        foreach (var graphic in sceneGraphics)
        {
            Debug.Log("Process: " + graphic.gameObject.name);
            graphics.Add(graphic.gameObject.name, graphic);
        }
    }

    private Dictionary<string, SpriteRenderer> graphics;
}
