using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// This script contains all the events that are referenced by the SceneTimeline
/// </summary>
public class LevelChapter1P2Events : LevelEvents
{
    public List<SpriteRenderer> sceneGraphics;
    public List<GameObject> shots;

    [SerializeField] private Button moveOnButton;
    [SerializeField] private SwipeInputHandler swipeInput;

    [SerializeField] private GameObject shot1, shot2;
    [SerializeField] private GameObject minigameA, minigameB, reply;
    [SerializeField] private Animator noteDeliver, noteDeliver2;
    [SerializeField] private GameObject noteHitbox, noteHitbox2;

    private void Start()
    {
        sfx.LoopSfx("lecture-ambience");
    }

    public void Shot1_1()
    {
        Debug.Log("Trigger shot 1-1");
        swipeInput.enabled = false;
        shot1.SetActive(false);
        shot2.SetActive(false);
        //StartCoroutine(Shot1_1_Progress());
    }

    public void OpenMinigameA()
    {
        minigameA.SetActive(true);
        sfx.PlaySfx("paper-fold");
    }

    public void OpenMinigameB()
    {
        minigameB.SetActive(true);
    }

    public void OpenReply()
    {
        reply.SetActive(true);
        sfx.PlaySfx("paper-fold");
        StartCoroutine(ReplyProgress());
    }

    public void NoteDelivery()
    {
        StartCoroutine(NoteDeliveryProgress());
    }

    public void NoteDelivery2()
    {
        StartCoroutine(NoteDelivery2Progress());
    }

    public void EndMinigames()
    {
        minigameA.SetActive(false);
        minigameB.SetActive(false);
        shot1.SetActive(false);
        shot2.SetActive(true);
        swipeInput.SetPosition(new Vector2(0, 0));
        StartCoroutine(Shot2_1_Progress());
    }

    public void EndPart()
    {
        sfx.PlaySfx("btn-tap-1");
        AppRoot.Instance.GetService<Transitioner>().TransitionToScene("Level_Chapter1_3");
    }

    private IEnumerator NoteDeliveryProgress()
    {
        noteHitbox.SetActive(false);
        yield return new WaitForSeconds(2f);
        noteDeliver.SetTrigger("NoteDeliver");
        noteHitbox.SetActive(true);
    }

    private IEnumerator NoteDelivery2Progress()
    {
        noteHitbox2.SetActive(false);
        yield return new WaitForSeconds(2f);
        noteDeliver2.SetTrigger("NoteDeliver");
        noteHitbox2.SetActive(true);
    }

    private IEnumerator Shot2_1_Progress()
    {
        swipeInput.enabled = false;
        SpriteRenderer s10 = graphics["Shot10_2"];
        yield return new WaitForSeconds(1f);
        FadeInGraphic(s10, 3f);
        swipeInput.enabled = true;
    }

    private IEnumerator ReplyProgress()
    {
        yield return new WaitForSeconds(8f);
        sfx.PlaySfx("possitive-confirm");
    }

    private void Awake()
    {
        graphics = new Dictionary<string, SpriteRenderer>();
        foreach (var graphic in sceneGraphics)
        {
            Debug.Log("Process: " + graphic.gameObject.name);
            graphics.Add(graphic.gameObject.name, graphic);
        }
    }

    private Dictionary<string, SpriteRenderer> graphics;
}
