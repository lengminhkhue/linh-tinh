using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// This script contains all the events that are referenced by the SceneTimeline
/// </summary>
public class LevelChapter1P3Events : LevelEvents
{
    public List<SpriteRenderer> sceneGraphics;
    public List<GameObject> shots;
    public ParallaxCamera camera;

    [SerializeField] private Button focusLikeButton;
    [SerializeField] private Image likeIcon;
    [SerializeField] private TMPro.TextMeshProUGUI textLike;
    [SerializeField] private Button friendReq;
    [SerializeField] private Sprite reqSent;
    [SerializeField] private Color likeColor;
    [SerializeField] private GameObject imageCanvas;

    private bool requestSent = false;

    public void Shot1_1()
    {
        Debug.Log("Trigger shot 1-1");
        //StartCoroutine(Shot1_1_Progress());
    }

    public void OnPressedLikeButton()
    {
        likeIcon.color = likeColor;
        textLike.color = likeColor;
        sfx.PlaySfx("message-pop-up");
    }

    public void OnPressedAddFriend()
    {
        friendReq.image.sprite = reqSent;
        requestSent = true;
        sfx.PlaySfx("add-friend");
    }

    public void OnPressedImage()
    {
        if (requestSent)
            imageCanvas.SetActive(true);
    }

    public void EndPart()
    {
        sfx.PlaySfx("btn-tap-1");
        AppRoot.Instance.GetService<Transitioner>().TransitionToScene("Level_Chapter1_4");
    }

    private void Start()
    {
        AppRoot.Instance?.GetService<BgmPlayer>().LoopBgm("bgm-on-social");
    }

    private void Awake()
    {
        graphics = new Dictionary<string, SpriteRenderer>();
        foreach (var graphic in sceneGraphics)
        {
            Debug.Log("Process: " + graphic.gameObject.name);
            graphics.Add(graphic.gameObject.name, graphic);
        }
    }

    private Dictionary<string, SpriteRenderer> graphics;
}
