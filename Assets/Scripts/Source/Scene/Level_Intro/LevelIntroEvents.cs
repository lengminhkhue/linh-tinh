using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// This script contains all the events that are referenced by the SceneTimeline
/// </summary>
public class LevelIntroEvents : LevelEvents
{
    public List<SpriteRenderer> sceneGraphics;
    public List<GameObject> shots;

    public DialougeManager dialogManager;
    public GameObject grandmaSpeech, grandsonSpeech, minigame;

    [SerializeField] private Button moveOnButton;

    private void Start()
    {
        moveOnButton.gameObject.SetActive(false);
        SpriteRenderer grandson = graphics["Shot1_Grandson"];
        grandson.gameObject.SetActive(false);
    }

    public void Shot1_1()
    {
        Debug.Log("Trigger shot 1-1");
        //SpriteRenderer bg = graphics["Shot1_Bg"];
        //FadeInGraphic(bg, 2f);

        //SpriteRenderer grandma = graphics["Shot1_Grandma"];
        //FadeInGraphic(grandma, 2f);
    }

    public void Shot1_2()
    {
        Debug.Log("Trigger shot 1-2");
        SpriteRenderer grandma = graphics["Shot1_Grandma"];
        //grandma.DOFade(0.2f, 0.1f).onComplete = () => grandma.DOFade(1f, 0.1f);
    }

    public void Shot1_3()
    {
        Debug.Log("Trigger shot 1-3");
        SpriteRenderer grandson = graphics["Shot1_Grandson"];
        grandson.gameObject.SetActive(true);

        grandson.transform.position = new Vector2(6, 0);
        grandson.transform.DOMoveX(1.9f, 2f).SetEase(Ease.OutSine);
    }

    public void Shot2_1()
    {
        Debug.Log("Trigger shot 2-1");

        shots.Find(x => x.name.Equals("Scene1_Shot2")).SetActive(true);

        SpriteRenderer bg = graphics["Shot2_Bg"];
        FadeInGraphic(bg, 1.5f);

        SpriteRenderer grandma = graphics["Shot2_Grandma"];
        FadeInGraphic(grandma, 1.5f);

        SpriteRenderer phone = graphics["Shot2_Phone"];
        FadeInGraphic(phone, 1.5f);
    }

    public void Shot2_2()
    {
        dialogManager.gameObject.SetActive(true);
        grandmaSpeech.SetActive(true);
        grandsonSpeech.SetActive(true);
    }

    public void Shot3_Minigame()
    {
        StartCoroutine(Shot3Progress());
    }

    private IEnumerator Shot3Progress()
    {
        shots.Find(x => x.name.Equals("Scene1_Minigame")).SetActive(true);

        dialogManager.gameObject.SetActive(false);
        grandmaSpeech.SetActive(false);
        grandsonSpeech.SetActive(false);
        minigame.SetActive(false);

        SpriteRenderer bg = graphics["Shot3_Bg"];
        FadeInGraphic(bg, 1.5f);

        yield return new WaitForSeconds(1.5f);

        moveOnButton.gameObject.SetActive(true);
        moveOnButton.onClick.AddListener(EndIntro);
    }

    private void EndIntro()
    {
        sfx.PlaySfx("btn-tap-1");
        AppRoot.Instance.GetService<Transitioner>().TransitionToScene("Level_Chapter1_1");
    }

    private void Awake()
    {
        graphics = new Dictionary<string, SpriteRenderer>();
        foreach (var graphic in sceneGraphics)
        {
            Debug.Log("Process: " + graphic.gameObject.name);
            graphics.Add(graphic.gameObject.name, graphic);
        }
    }

    private Dictionary<string, SpriteRenderer> graphics;
}
