using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;

[System.Serializable]
public class MinigameAConfig
{
    public float duration;

    [TextArea] public string optionA;
    [TextArea] public string optionB;
    [TextArea] public string optionC;
    [TextArea] public string optionD;
}

public class MinigameA : MonoBehaviour
{
    public List<MinigameAConfig> phases;
    public UnityEvent endGameEvent;
    public UnityEvent selectA, selectB, selectC, selectD;

    [Header("DO NOT touch this!")]
    [SerializeField] private TMPro.TextMeshProUGUI optA, optB, optC, optD;
    [SerializeField] private Image countdown;
    [SerializeField] private Image paper;
    [SerializeField] private Sprite[] noteProgression;
    private int currentPhaseIndex;
    private float timer = 0f;
    private float duration = 1f;
    private bool activated = false;

    private IEnumerator Start()
    {
        currentPhaseIndex = -1;
        activated = false;
        yield return new WaitForSeconds(1.5f);
        activated = true;
        MoveOn();
    }

    private void Update()
    {
        if (!activated)
            return;

        if (currentPhaseIndex >= phases.Count)
            return;

        timer += Time.deltaTime;
        var scale = countdown.transform.localScale;
        scale.x = 1f - (timer / duration);
        countdown.transform.localScale = scale;
        var config = phases[currentPhaseIndex];

        if (timer >= duration)
        {
            MoveOn();
        }
    }

    public void ReloadConfig(MinigameAConfig config)
    {
        timer = 0;
        duration = config.duration;
        optA.text = config.optionA;
        optB.text = config.optionB;
        optC.text = config.optionC;
        optD.text = config.optionD;

        optA.gameObject.SetActive(!string.IsNullOrEmpty(optA.text));
        optB.gameObject.SetActive(!string.IsNullOrEmpty(optB.text));
        optC.gameObject.SetActive(!string.IsNullOrEmpty(optC.text));
        optD.gameObject.SetActive(!string.IsNullOrEmpty(optD.text));
    }

    public void MoveOn()
    {
        currentPhaseIndex++;
        if (currentPhaseIndex >= phases.Count)
        {
            endGameEvent.Invoke();
        }
        else
        {
            ReloadConfig(phases[currentPhaseIndex]);
            paper.sprite = noteProgression[currentPhaseIndex];
        }
    }

    public void OnClickButtonA()
    {
        selectA.Invoke();
        MoveOn();
    }

    public void OnClickButtonB()
    {
        selectB.Invoke();
        MoveOn();
    }

    public void OnClickButtonC()
    {
        selectC.Invoke();
        MoveOn();
    }

    public void OnClickButtonD()
    {
        selectD.Invoke();
        MoveOn();
    }

    public void TestOnEndGameEvent()
    {
        Debug.Log("End minigame A");
    }
}
