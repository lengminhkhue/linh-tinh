using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;

public enum SlideDirection
{
    Left = -1,
    Right = 1
}

[System.Serializable]
public class MinigameConfig
{
    public float safeZoneLeft, safeZoneRight;
    public float slideSpeed;
    [Range(0f, 1f)] public float initialValue;
    public SlideDirection initialDirection;
}

public class MinigameB : MonoBehaviour
{
    public List<MinigameConfig> phases;
    public UnityEvent onMissedClickEvent;
    public UnityEvent endGameEvent;

    [Header("DO NOT touch this!")]
    [SerializeField] private Slider slider;
    [SerializeField] private Image safeZone;
    [SerializeField] private Image slideZone;
    [SerializeField] private TMPro.TextMeshProUGUI missedText;
    [SerializeField] private CanvasGroup instruction;

    private float slideSpeed;
    private float safeMin, safeMax;
    private int slideDirection;
    private int currentPhaseIndex;

    public void ReloadConfig(MinigameConfig config)
    {
        safeZone.gameObject.SetActive(true);
        slider.value = config.initialValue;
        slideDirection = (int)config.initialDirection;
        slideSpeed = config.slideSpeed;

        var offsetMin = safeZone.rectTransform.offsetMin;
        var offsetMax = safeZone.rectTransform.offsetMax;
        offsetMin.x = config.safeZoneLeft;
        offsetMax.x = -config.safeZoneRight;
        safeZone.rectTransform.offsetMin = offsetMin;
        safeZone.rectTransform.offsetMax = offsetMax;

        var size = slideZone.rectTransform.rect.width;
        safeMin = safeZone.rectTransform.offsetMin.x / size;
        safeMax = (safeZone.rectTransform.offsetMax.x + size) / size;

        Debug.Log("Safe min, max = " + safeMin + ", " + safeMax + "/" + size + "/" + safeZone.rectTransform.offsetMin.x + "/" + safeZone.rectTransform.offsetMax.x);
    }

    public void TestEndGameEvent()
    {
        Debug.Log("Test: end minigame");
    }

    public void TestOnMissedClickedEvent()
    {
        Debug.Log("Test: missed tap");
        missedText.rectTransform.localScale = Vector3.one;
        missedText.rectTransform.DOScale(0f, 1f).SetEase(Ease.InExpo);
    }

    private IEnumerator Start()
    {
        currentPhaseIndex = 0;
        missedText.rectTransform.localScale = Vector3.zero;
        safeZone.gameObject.SetActive(false);

        yield return new WaitForSeconds(6f);
        ReloadConfig(phases[currentPhaseIndex]);
    }

    private void Update()
    {
        slider.value += slideDirection * slideSpeed * Time.deltaTime;
        if (slider.value <= slideSpeed * Time.deltaTime || slider.value >= 1f - slideSpeed * Time.deltaTime)
        {
            slideDirection *= -1;
        }

        if (safeMin <= slider.value && slider.value <= safeMax)
        {
            if (Input.GetMouseButtonDown(0))
            {
                currentPhaseIndex++;
                if (currentPhaseIndex >= phases.Count)
                {
                    endGameEvent.Invoke();
                    enabled = false;
                }
                else
                {
                    ReloadConfig(phases[currentPhaseIndex]);
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                onMissedClickEvent.Invoke();
            }
        }
    }
}
