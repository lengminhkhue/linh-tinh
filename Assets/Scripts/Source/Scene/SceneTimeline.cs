using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[System.Serializable]
public class EventData
{
    public float timeStamp;
    public UnityEvent action;
    public bool pausingEvent;

    [HideInInspector] public bool isTriggered = false;
}

public class SceneTimeline : MonoBehaviour
{
    public List<EventData> events;

    private float timer;
    private int currentEventIndex;

    public void UnpauseCurrentEvent()
    {
        if (currentEventIndex >= events.Count)
            return;

        EventData currentEvent = events[currentEventIndex];
        currentEvent.pausingEvent = false;
    }

    private void Awake()
    {
        events.Sort((EventData a, EventData b) => (int)(a.timeStamp - b.timeStamp));
    }

    private void Start()
    {
        timer = 0;
        currentEventIndex = 0;
    }

    private void Update()
    {
        if (currentEventIndex >= events.Count)
            return;

        EventData currentEvent = events[currentEventIndex];
        if (!currentEvent.pausingEvent)
            timer += Time.deltaTime;

        SeekCurrentEventIndex();
    }

    private void SeekCurrentEventIndex()
    {
        EventData currentEvent = events[currentEventIndex];
        bool isDone = false;
        while (!isDone && currentEvent.timeStamp <= timer && currentEventIndex < events.Count)
        {
            isDone = true;
            if (!currentEvent.isTriggered)
            {
                currentEvent.action.Invoke();
                currentEvent.isTriggered = true;
                currentEventIndex++;
                isDone = false;
            }
        }
    }
}
