using Framework.ServiceLocator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Transitioner : Service
{
    [SerializeField] private Image mask;

    public void TransitionToScene(string targetSceneName, float fadeDuration = 1f, float waitDuration = 1f)
    {
        StartCoroutine(TransitionProcess(targetSceneName, fadeDuration, waitDuration));
    }

    private IEnumerator TransitionProcess(string targetScene, float fadeDuration, float waitDuration)
    {
        mask.gameObject.SetActive(true);
        mask.CrossFadeAlpha(1f, fadeDuration, true);

        yield return new WaitForSeconds(fadeDuration);

        yield return SceneManager.LoadSceneAsync(targetScene);
        yield return new WaitForSeconds(waitDuration);

        mask.CrossFadeAlpha(0f, fadeDuration, true);
        yield return new WaitForSeconds(fadeDuration);

        mask.gameObject.SetActive(false);
    }
}
