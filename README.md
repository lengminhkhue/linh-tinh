# Linh Tinh: A Cozy Witch's House Enchantic Journey

Welcome to Linh Tinh, a charming 2D puzzle game that takes you on an enchanting journey to a Vietnamese rural area where you, an apprentice, are tasked with cleaning a witch's house. This cozy and chill game combines point-and-click gameplay with object organization as you explore the witch's nook.

![Gameplay Screenshot](screenshots/gameplay.png)

## About the Game

In Linh Tinh, you step into the shoes of a humble apprentice who has been given the important job of tidying up a witch's magical nook. Embrace the laid-back atmosphere of rural Vietnam as you:

- **Organize Objects:** Use your mouse to drag and arrange objects around the witch's house.
- **Solve Puzzles:** Interact with various items and solve clever puzzles to uncover hidden secrets, story behind the madam.
- **Change Object Status:** Click and point to change the status of objects, revealing their magical properties.

## Installation

1. Clone this repository to your local machine using `git clone`.
2. Ensure you have [Python](https://www.python.org/) installed.
3. Install required dependencies by running `pip install -r requirements.txt`.
4. Launch the game by running `python linh_tinh.py`.

## Gameplay

- Navigate through rooms and basement as you explore the witch's nook.
- Click and drag objects to reorganize and uncover hidden surprises.
- Solve puzzles by interacting with items and using your wit to progress.
- Immerse yourself in the cozy atmosphere and delightful soundtrack.

## System Requirements

- OS: Windows 10, macOS 10.14, or Linux
- Processor: Dual-core 2 GHz or equivalent
- Memory: 4 GB RAM
- Graphics: DirectX 10 compatible graphics card
- Storage: 500 MB available space

## Contributing

We appreciate contributions from the community! If you'd like to contribute, please follow our [Contributor Guidelines](CONTRIBUTING.md).

## License

Linh Tinh is licensed under the [MIT License](LICENSE).

## Credits

- Developed by Keisuke, Helen Ly, and Mathew Tran
- Artwork and design by [ArtistName](https://www.artistwebsite.com)
- Music and sound effects by [MusicianName](https://www.musicianwebsite.com)

## Changelog

### Version 1.0.0 (Release Date: December 19, 2023)

- Initial release of Linh Tinh: A Cozy Witch's House Cleaning Adventure.
